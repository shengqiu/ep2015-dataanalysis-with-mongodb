# Data Analysis and Map-Reduce with mongoDB and Pymongo
## iPython Notebook for my talk at Europython 2015, Bilbao

![video](videopreview.png)

The live recording of the 45 minute talk can be found [here on YouTube](https://youtu.be/5djvv5-zgnQ?t=5m48s "Data Analysis with mongoDB")

Slides can be downloaded from [the EuroPython 2015 website](https://ep2015.europython.eu/media/conference/slides/data-analysis-and-map-reduce-with-mongodb-and-pymongo.pdf "Data Analysis with mongoDB")

Due to some user requests I have created this simple git to provide the sample data 
and the iPython notebook file.

This is not meant to a full tutorial i.e. setup might be buggy. 
No support for setting things up. Google is your friend.


### requirements:

* mongodb installed (version 3.0+)    
* python 3.4 (everything should also work on 2.7, but not tested)

the data is located in data/

don't forget to install the required python modules, e.g. with

    pip install -r requirements.txt
    
if you want to use an virtualenv (recommended) or not is up to you.
    
please note the Natutal Language Tool Kit (nltk) install is optional, 
you do not need to worry about installing any extra nltk libraries.


having everything in place:

    sh start.sh
    
should start your mongod instance.

These tips are Mac OS X only, other OS may be different.



