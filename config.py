"""
presets artist & nemesis
create sample document simplified_document in ordered fashion
"""
from collections import OrderedDict

artist = "Taylor Swift"
nemesis = "Katy Perry"

# if nltk is not fully installed, fallback to these stopwords
_stopwords = ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', 'your', 'yours', 'yourself',
              'yourselves', 'he', 'him', 'his', 'himself', 'she', 'her', 'hers', 'herself', 'it', 'its', 'itself',
              'they', 'them', 'their', 'theirs', 'themselves', 'what', 'which', 'who', 'whom', 'this', 'that', 'these',
              'those', 'am', 'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do',
              'does', 'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while',
              'of', 'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into', 'through', 'during', 'before',
              'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off', 'over', 'under', 'again',
              'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how', 'all', 'any', 'both', 'each',
              'few', 'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'not', 'only', 'own', 'same', 'so', 'than',
              'too', 'very', 's', 't', 'can', 'will', 'just', 'don', 'should', 'now']

d = {"_id": "ObjectId(5215d7f3ee6da1070d5cb88a)", "adamId": 573885160}
info = {"artistId": 358714030, "artistIdsIndex": 358714030, "artistName": "Imagine Dragons",
        "name": "Night Visions", "offers": [{"priceFormatted": "USD 9.99", "price": 9.99, }],
        "releaseDate": "2013-02-01", "releaseDateEpoch": "ISODate('2013-02-01T00:00:00Z')",
        "userRating": {"value": 5, "ratingCount": 8}}
children = [
    {"kind": "song", "name": "Amsterdam", "releaseDate": "2013-02-01", "artistId": 358714030,
     "offers": [{"assets": [{"duration": 194}], "priceFormatted": "USD 0.99",
                 "price": 0.99, }], }
]
simplified_document = OrderedDict()
for k in sorted(d.keys()):
    simplified_document[k] = d[k]
simplified_document['info'] = {}
for k in sorted(info.keys()):
    simplified_document['info'][k] = info[k]
c = OrderedDict()
for k in sorted(children[0].keys()):
    c[k] = children[0][k]
simplified_document['info']['children'] = [c]
